import { path } from 'path'

module.exports = {
  resolve: {
    root: path.resolve('./'),
  },
}
