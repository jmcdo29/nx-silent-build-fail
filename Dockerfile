FROM node:16.5.0-alpine

WORKDIR /usr/app

COPY ./ /usr/app/

RUN npm install
RUN npm run build -- silent-build-fail

CMD ["node", "dist/apps/silent-build-fail/main"]
